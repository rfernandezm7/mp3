/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MP3.Dao;
import java.sql.SQLException;
import java.util.List;

public interface TrackPlaylistDao<T, ID1, ID2> {
    void add(ID1 id1, ID2 id2) throws SQLException;
    void remove(ID1 id1, ID2 id2) throws SQLException;
    List<T> getAll(ID1 id) throws SQLException;
    boolean isTrackInPlaylist(ID1 id1, ID2 id2) throws SQLException;
}

    // Other generic CRUD operations if needed







