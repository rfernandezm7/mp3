package MP3.Dao;

import MP3.Model.Playlist;
import MP3.Pool.MyDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;

public class PlaylistDaoImpl implements PlaylistDao<Playlist> {

    private static PlaylistDaoImpl instance;
    private DataSource dataSource;

    private PlaylistDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private PlaylistDaoImpl() {
    }

    public static PlaylistDaoImpl getInstance() {
        if (instance == null) {
            instance = new PlaylistDaoImpl((DataSource) MyDataSource.getDataSource());
        }
        return instance;
    }

    @Override
    public List<Playlist> getAll() throws SQLException {
        List<Playlist> playlists = new ArrayList<>();
        String selectQuery = "SELECT * FROM Playlists";

        try ( Connection connection = MyDataSource.getConnection();  PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);  ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                String name = resultSet.getString("name");
                java.sql.Date creationDate = resultSet.getDate("creation");
                String observations = resultSet.getString("observations");
                String icon = resultSet.getString("icon");

                Playlist playlist = new Playlist(name, creationDate, observations, icon);
                playlists.add(playlist);
            }
        }

        return playlists;
    }

    @Override
    public void create(Playlist playlist) throws SQLException {
        String sql = "INSERT INTO Playlists (name, creation, observations, icon) VALUES (?, NOW(), ?, ?)";

        try ( Connection connection = MyDataSource.getConnection();  PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, playlist.getName());
           // preparedStatement.setDate(2, new java.sql.Date(playlist.getCreation().getTime()));
            preparedStatement.setString(2, playlist.getObservations());
            preparedStatement.setString(3, playlist.getIcon());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public void delete(String playlistName) throws SQLException {
        String sql = "DELETE FROM Playlists WHERE name = ?";

        try ( Connection connection = MyDataSource.getConnection();  PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, playlistName);
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public void update(Playlist playlist, String originalName) throws SQLException {
        String updateQuery = "UPDATE Playlists SET name = ?, icon = ?, observations = ? WHERE name = ?";

        try ( Connection connection = MyDataSource.getConnection();  PreparedStatement preparedStatement = connection.prepareStatement(updateQuery)) {
            preparedStatement.setString(1, playlist.getName());
            preparedStatement.setString(2, playlist.getIcon());
            preparedStatement.setString(3, playlist.getObservations());
            preparedStatement.setString(4, originalName);

            preparedStatement.executeUpdate();
        }
        
    }
    @Override
    public Playlist findByName(String name) throws SQLException {
        Playlist playlist = null;

        try (Connection connection = MyDataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Playlists WHERE name = ?")) {
            preparedStatement.setString(1, name);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    // La lista de reproducción fue encontrada
                    String playlistName = resultSet.getString("name");
                    Date creationDate = resultSet.getDate("creation");
                    String observations = resultSet.getString("observations");
                    String icon = resultSet.getString("icon");

                    // Crea la instancia de Playlist con el constructor que acepta varios atributos
                    playlist = new Playlist(playlistName, creationDate, observations, icon);
                }
            }
        } catch (SQLException e) {
            // Manejar la excepción según tus necesidades
            e.printStackTrace();
            throw e;
        }

        return playlist;
    }
}

