package MP3.Dao;


import java.sql.SQLException;
import java.util.List;



public interface PlaylistDao<T> {
    void create(T t) throws SQLException;
    void delete(String t) throws SQLException;
    List<T> getAll() throws SQLException;
    void update(T t, String originalName) throws SQLException;
    T findByName(String name) throws SQLException;

    // Other CRUD operations
}

