package MP3.Dao;

import MP3.Dao.TrackDao;
import MP3.Model.Track;
import MP3.Pool.MyDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class TrackDaoImpl implements TrackDao<Track, Integer> {


    
     //Declararem una variable estàtica perquè poder implementar un patró singleton  
    public static final TrackDaoImpl instance;

    static { //instanciem l'únic objecte d'aquesta classe
        instance = new TrackDaoImpl();
    }

    public TrackDaoImpl() {
    } //el declarem privat perquè ningú el pugui cridar

    public static TrackDaoImpl getInstance() {
        return instance;
    }
    
  

    @Override
    public void delete(Integer trackHash) throws SQLException {
    String deleteQuery = "DELETE FROM Tracks WHERE track_hash = ?";

    try (Connection connection = MyDataSource.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement(deleteQuery)) {
        preparedStatement.setInt(1, trackHash);
        preparedStatement.executeUpdate();
    }
}

    
    @Override
    public List<Track> getAll() throws SQLException {
    List<Track> tracks = new ArrayList<>();
    String selectQuery = "SELECT * FROM Tracks";

    try (Connection connection = MyDataSource.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
         ResultSet resultSet = preparedStatement.executeQuery()) {

        while (resultSet.next()) {
            int trackHash = resultSet.getInt("track_hash");
            String title = resultSet.getString("title");
            String artist = resultSet.getString("artist");
            byte[] icon = resultSet.getBytes("icon");
            String observation = resultSet.getString("observation");
            String folderPath = resultSet.getString("folder_path");
            int height = resultSet.getInt("heigth");
            int width = resultSet.getInt("width");

            Track track = new Track(trackHash, title, artist, icon, observation, folderPath, height, width);
            tracks.add(track);
        }
    }

    return tracks;
}
    public void add(Track track) throws SQLException {
        String insertQuery = "INSERT INTO Tracks (track_hash, title, artist, icon, observation, folder_path, heigth, width) VALUES (?, ?, ?, ?, ?, ?, ? ,? )";

        try (Connection connection = MyDataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(insertQuery)) {
            
            // Configurar los parámetros del PreparedStatement
            preparedStatement.setInt(1, track.hashCode());
            preparedStatement.setString(2, track.getTitle());
            preparedStatement.setString(3, track.getArtist());
            preparedStatement.setBytes(4, track.getIcon());
            preparedStatement.setString(5, track.getObservation());
            preparedStatement.setString(6, track.getFolderPath());
            preparedStatement.setInt(7, track.getHeight());
            preparedStatement.setInt(8, track.getWidth());
            preparedStatement.executeUpdate();
        }
    }
    @Override
    public ObservableList<Track> searchFilter(String keyword) throws SQLException {
    ObservableList<Track> filteredTracks = FXCollections.observableArrayList();
        String filterQuery = "SELECT * FROM Tracks WHERE title LIKE ? OR artist LIKE ?";

        try (Connection connection = MyDataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(filterQuery)) {

            // Configura los parámetros del PreparedStatement
            preparedStatement.setString(1, "%" + keyword + "%");
            preparedStatement.setString(2, "%" + keyword + "%");

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    int trackHash = resultSet.getInt("track_hash");
                    String title = resultSet.getString("title");
                    String artist = resultSet.getString("artist");
                    byte[] icon = resultSet.getBytes("icon");
                    String observation = resultSet.getString("observation");
                    String folderPath = resultSet.getString("folder_path");
                    int height = resultSet.getInt("heigth");
                    int width = resultSet.getInt("width");

                    Track track = new Track(trackHash, title, artist, icon, observation, folderPath, height, width);
                    filteredTracks.add(track);
                }
            }
        }

        return filteredTracks;
    }
}