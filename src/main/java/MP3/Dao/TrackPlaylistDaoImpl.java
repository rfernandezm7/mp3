/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MP3.Dao;

import MP3.Model.TrackPlaylist;
import MP3.Pool.MyDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Yassine Ainous
 */
public class TrackPlaylistDaoImpl implements TrackPlaylistDao<TrackPlaylist, String, Integer> {

     //Declararem una variable estàtica perquè poder implementar un patró singleton  
    private static final TrackPlaylistDaoImpl instance;

    static { //instanciem l'únic objecte d'aquesta classe
        instance = new TrackPlaylistDaoImpl();
    }

    public TrackPlaylistDaoImpl() {
    } //el declarem privat perquè ningú el pugui cridar

    public static TrackPlaylistDaoImpl getInstance() {
        return instance;
    }
    
    @Override
    public void remove(String playlistName, Integer trackHash) throws SQLException {
    String deleteQuery = "DELETE FROM Tracks_playlist WHERE name = ? AND track_hash = ?";

    try (Connection connection = MyDataSource.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement(deleteQuery)) {
        preparedStatement.setString(1, playlistName);
        preparedStatement.setInt(2, trackHash);
        preparedStatement.executeUpdate();
    }
}

    
    
    @Override
    public void add(String playlistName, Integer trackHash) throws SQLException {
    String insertQuery = "INSERT INTO Tracks_playlist (name, track_hash) VALUES (?, ?)";

    try (Connection connection = MyDataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(insertQuery)) {
        preparedStatement.setString(1, playlistName);
        preparedStatement.setInt(2, trackHash);
        preparedStatement.executeUpdate();
    }
}
    
    @Override
    public List<TrackPlaylist> getAll(String playlistName) throws SQLException {
    List<TrackPlaylist> trackPlaylists = new ArrayList<>();
    String selectQuery = "SELECT * FROM Tracks_playlist WHERE name = ?";

    try (Connection connection = MyDataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery)) {
        preparedStatement.setString(1, playlistName);

        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                int trackHash = resultSet.getInt("track_hash");
                TrackPlaylist trackPlaylist = new TrackPlaylist(playlistName, trackHash);
                trackPlaylists.add(trackPlaylist);
            }
        }
    }

    return trackPlaylists;
}
    @Override
    public boolean isTrackInPlaylist(String playlistName, Integer trackHash) throws SQLException {
        String selectQuery = "SELECT * FROM Tracks_playlist WHERE name = ? AND track_hash = ?";
        boolean trackInPlaylist = false;

        try (Connection connection = MyDataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(selectQuery)) {
            preparedStatement.setString(1, playlistName);
            preparedStatement.setInt(2, trackHash);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                trackInPlaylist = resultSet.next();
            }
        }

        return trackInPlaylist;
    }


}

