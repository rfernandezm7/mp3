/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MP3.Dao;

import java.sql.SQLException;
import java.util.List;
import javafx.collections.ObservableList;

/**
 *
 * @author Yassine Ainous
 * @param <T>
 * @param <ID>
 */
public interface TrackDao<T, ID> {
    void delete(ID id ) throws SQLException;
    List<T> getAll() throws SQLException;
    ObservableList<T> searchFilter(String keyword) throws SQLException;
    // Other CRUD operations
}
