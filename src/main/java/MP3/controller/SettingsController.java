package MP3.controller;


import static MP3.utils.ConfigManager.*;
import MP3.utils.FileUtils;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.Window;

public class SettingsController {

    @FXML
    private Pane rootPane;
    @FXML
    private ToggleButton btn_toggle_Colorblind;
    @FXML
    private ToggleButton btn_toggle_Dark;
    @FXML
    private RadioButton btn_radio_Colorblind1;
    @FXML
    private RadioButton btn_radio_Colorblind2;
    @FXML
    private RadioButton btn_radio_Colorblind3;
    @FXML
    private RadioButton btn_radio_Fontsize12;
    @FXML
    private RadioButton btn_radio_Fontsize16;
    @FXML
    private RadioButton btn_radio_Fontsize20;
    @FXML
    private Button btn_apply;

    // Grupo para los RadioButtons de las opciones de daltonismo
    @FXML
    private ToggleGroup darkblindgroups = new ToggleGroup();
    @FXML
    private ToggleGroup colorblindOptions = new ToggleGroup();
    @FXML
    private ToggleGroup fontsizeOptions = new ToggleGroup();

    int colorblind;
    boolean darkmode;
    int fontsize = 16;

    @FXML
    public void initialize() {
        try { // Load the configuration
            loadConfg();
            colorblind = Integer.parseInt(getConfg("colorblind"));
            darkmode = Boolean.parseBoolean(getConfg("darkmode"));
            fontsize = Integer.parseInt(getConfg("fontsize"));

            // Selects by the stored values
            if (darkmode) {
                btn_toggle_Dark.setSelected(true);
                toggle_darkmode();
            } else {
                if (colorblind == 0) {
                    btn_toggle_Colorblind.setSelected(false);
                } else {
                    btn_toggle_Colorblind.setSelected(true);
                    toggle_colorblind();
                    switch (colorblind) {
                        case 1 -> {
                            btn_radio_Colorblind1.setSelected(true);
                        }
                        case 2 -> {
                            btn_radio_Colorblind2.setSelected(true);
                        }
                        case 3 -> {
                            btn_radio_Colorblind3.setSelected(true);
                        }
                    }
                }
            }
            // Selects by the stored value
            switch (fontsize) {
                case 12 -> {
                    btn_radio_Fontsize12.setSelected(true);
                }
                case 16 -> {
                    btn_radio_Fontsize16.setSelected(true);
                }
                case 20 -> {
                    btn_radio_Fontsize20.setSelected(true);
                }
            }

            btn_radio_Colorblind1.setUserData("1");
            btn_radio_Colorblind2.setUserData("2");
            btn_radio_Colorblind3.setUserData("3");

            btn_radio_Fontsize12.setUserData("12");
            btn_radio_Fontsize16.setUserData("16");
            btn_radio_Fontsize20.setUserData("20");
        } catch (IOException e) {
            FileUtils.mostrarMensajeError("Error de archivo al hacer la adaptacion visual");
        }
    }

    public void toggle_colorblind() {
        if (btn_toggle_Colorblind.isSelected()) {
            btn_toggle_Colorblind.setText("Disable");

            btn_radio_Colorblind1.setDisable(false);
            btn_radio_Colorblind2.setDisable(false);
            btn_radio_Colorblind3.setDisable(false);
            switch (colorblind) {
                case 1 -> {
                    btn_radio_Colorblind1.setSelected(true);
                }
                case 2 -> {
                    btn_radio_Colorblind2.setSelected(true);
                }
                case 3 -> {
                    btn_radio_Colorblind3.setSelected(true);
                }
            }
            // Disable darkmode button and value
            btn_toggle_Dark.setDisable(true);
            toggle_darkmode();
        } else {
            btn_toggle_Colorblind.setText("Enable");
            colorblind = 0;

            btn_radio_Colorblind1.setSelected(false);
            btn_radio_Colorblind2.setSelected(false);
            btn_radio_Colorblind3.setSelected(false);
            btn_radio_Colorblind1.setDisable(true);
            btn_radio_Colorblind2.setDisable(true);
            btn_radio_Colorblind3.setDisable(true);

            btn_toggle_Dark.setDisable(false);
        }
    }

    public void toggle_darkmode() {
        if (btn_toggle_Dark.isSelected()) {
            btn_toggle_Dark.setText("Disable");
            darkmode = true;

            btn_toggle_Colorblind.setDisable(true);
            btn_toggle_Colorblind.setSelected(false);
            toggle_colorblind();
        } else {
            btn_toggle_Dark.setText("Enable");
            darkmode = false;

            btn_toggle_Colorblind.setDisable(false);
        }
    }

    public void saveChanges() throws IOException {
        try {
            // We save the values for the application
            saveConfg("colorblind", String.valueOf(applyGroupColorblind()));
            saveConfg("darkmode", String.valueOf(darkmode));
            saveConfg("fontsize", String.valueOf(applyGropuFontsize()));

            // conseguir de forma dinamica
            for (Window window : Window.getWindows()) {
                if (window instanceof Stage stage) {
                    Scene scene = stage.getScene();
                    if (scene != null) {
                        scene.getStylesheets().clear();
                        if (darkmode) {
                            scene.getStylesheets().add("/style/CSSdarkmode.css");
                        } else {
                            switch (applyGroupColorblind()) {
                                case 0 -> {
                                    scene.getStylesheets().add("/style/CSSstandard.css");
                                }
                                case 1 -> {
                                    scene.getStylesheets().add("/style/CSScolorblindD.css");
                                }
                                case 2 -> {
                                    scene.getStylesheets().add("/style/CSScolorblindP.css");
                                }
                                case 3 -> {
                                    scene.getStylesheets().add("/style/CSScolorblindT.css");
                                }
                            }
                        }
                        switch (applyGropuFontsize()) {
                            case 12 -> {
                                scene.getStylesheets().add("/style/fontsize/CSSfontsize12.css");
                            }
                            case 16 -> {
                                scene.getStylesheets().add("/style/fontsize/CSSfontsize16.css");
                            }
                            case 20 -> {
                                scene.getStylesheets().add("/style/fontsize/CSSfontsize20.css");
                            }
                        }
                    }
                }
            }

        } catch (IOException e) {
            throw e;
        }
    }

    public int applyGroupColorblind() {
        int colorblindvalue;
        if (colorblindOptions.getSelectedToggle() != null) {
            // Cast object to radio button
            RadioButton iDontKnow = (RadioButton) colorblindOptions.getSelectedToggle();
            colorblindvalue = Integer.parseInt(iDontKnow.getUserData().toString());
        } else {
            colorblindvalue = 0;
        }
        return colorblindvalue;
    }

    public int applyGropuFontsize() {
        int fontsizevalue;
        if (fontsizeOptions.getSelectedToggle() != null) {
            // Cast object to radio button
            RadioButton iDontKnow = (RadioButton) fontsizeOptions.getSelectedToggle();
            fontsizevalue = Integer.parseInt(iDontKnow.getUserData().toString());
        } else {
            fontsizevalue = 12;
        }
        return fontsizevalue;
    }
}
