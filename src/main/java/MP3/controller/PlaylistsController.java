package MP3.controller;

import MP3.Dao.PlaylistDao;
import MP3.Dao.PlaylistDaoImpl;
import MP3.Model.Playlist;
import MP3.controller.CreatePlaylistDialogController;
import MP3.controller.ModifyPlaylistDialogController;
import MP3.utils.FileUtils;
import MP3.utils.ViewAlterators;
import static MP3.utils.ViewAlterators.loadCSS;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class PlaylistsController {
    // Asumimos que estos son los botones que abren los popups

    @FXML
    private ObservableList<Playlist> playlists;
    @FXML
    private Button createPlaylistButton;
    @FXML
    private Button modifyPlaylistButton;
    @FXML
    private Button deleteButton;
    @FXML
    private TextField nameField;
    @FXML
    private TableColumn<Playlist, Date> columnaCreation;
    @FXML
    private TextField observationField;
    @FXML
    private TextField done;
    @FXML
    private TextField search;
    @FXML
    private ImageView imageView; // Tendrás un ImageView para mostrar el icono seleccionado
    @FXML
    private Button backButton; // Variable para el botón
    @FXML
    private ContextMenu cmp;
    @FXML
    private ContextMenu cmp_1;
    @FXML
    private ContextMenu cmp_2;
    @FXML
    private TableView<Playlist> playlistTable;
    @FXML
    private TableColumn<Playlist, String> columnaNombre;
    @FXML
    private TableColumn<Playlist, String> columnaObservaciones;
    @FXML
    private TableColumn<Playlist, ImageView> columnaIcono;

    private ObservableList<Playlist> allPlaylists;
    private PlaylistDao playlistDao;

    public PlaylistsController() {
        this.playlistDao = PlaylistDaoImpl.getInstance();
    }

    @FXML
    private void initialize() {
        columnaNombre.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getName()));
        columnaObservaciones.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getObservations()));
        columnaIcono.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(createImageView(cellData.getValue().getIcon())));
        columnaCreation.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getCreation()));
        //playlistTable.setItems(allPlaylists); // Vincula la TableView con la lista observable una sola vez

        playlists = FXCollections.observableArrayList();
        allPlaylists = FXCollections.observableArrayList();

        // Configura las columnas del TableView aquí...
        loadPlaylistsFromDatabase(); // Suponiendo que esta función llena allPlaylists
        
        playlistTable.setItems(allPlaylists);

        // Añadir listener a search
        search.textProperty().addListener((observable, oldValue, newValue) -> {
            filterPlaylists(newValue);
        });
    }

    private void filterPlaylists(String searchText) {
        if (searchText == null || searchText.isEmpty()) {
            playlistTable.setItems(allPlaylists);
        } else {
            ObservableList<Playlist> filteredList = allPlaylists.filtered(playlist
                    -> playlist.getName().toLowerCase().contains(searchText.toLowerCase())
            );
            playlistTable.setItems(filteredList);
        }
    }

    private void loadPlaylistsFromDatabase() {
        try {
            List<Playlist> fetchedPlaylists = playlistDao.getAll();
            allPlaylists = FXCollections.observableArrayList(fetchedPlaylists);
            playlistTable.setItems(allPlaylists);
        } catch (SQLException e) {
            e.printStackTrace();
            // Manejar la excepción
        }
    }

    private ImageView createImageView(String url) {
        if (url == null || url.isEmpty()) {
            return new ImageView(); // ImageView vacío o con imagen predeterminada
        }
        try {
            ImageView imageView = new ImageView(new Image(url, true));
            imageView.setFitHeight(30); // Ajusta según sea necesario
            imageView.setPreserveRatio(true);
            return imageView;
        } catch (Exception e) {
            e.printStackTrace();
            return new ImageView(); // En caso de error, devuelve un ImageView vacío
        }
    }

    @FXML
    private void switchToMainView(ActionEvent event) {
        // Lógica para cambiar a la vista MainView.
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/main/MainView.fxml"));
        ViewAlterators.switchView(loader, event);
    }

    @FXML
    public void changetoSettings(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/additionals/SettingsView.fxml"));
        ViewAlterators.newView(loader, "Settings", 600, 300);
    }

    @FXML
    private void showCreatePlaylistPopup() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/additionals/CreatePlaylistDialog.fxml"));
        Parent root = loader.load();

        CreatePlaylistDialogController popupController = loader.getController();
        popupController.setPlaylistsController(this); // Establece la referencia aquí

        Stage popupStage = new Stage();

        popupStage.initModality(Modality.APPLICATION_MODAL);
        popupStage.setTitle("Create Playlist");
        popupStage.setScene(new Scene(root));
        popupStage.showAndWait();
    }

    // Método para mostrar el popup de modificación de playlist
    @FXML
    private void showModifyPlaylistPopup() throws IOException {
        Playlist selectedPlaylist = playlistTable.getSelectionModel().getSelectedItem();
        if (selectedPlaylist != null) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/additionals/ModifyPlaylistDialog.fxml"));
            Parent root = loader.load();

            ModifyPlaylistDialogController modifyController = loader.getController();
            modifyController.setPlaylist(selectedPlaylist);
            modifyController.setPlaylistsController(this);

            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("Modify Playlist");
            stage.setScene(new Scene(root));
            stage.showAndWait();

            // Refrescar la tabla
            refreshTable();
        } else {
            FileUtils.mostrarMensajeError("Please select a playlist to modify.");
        }
    }

// Método para refrescar la tabla
    public void refreshTable() {
        loadPlaylistsFromDatabase(); // Este método debería volver a cargar la lista desde la base de datos
    }

// Este método se llamará cuando se haga clic en el botón "Buscar icono"
    @FXML
    private void handleBrowse() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select Icon");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Archivos de Imagen", "*.png", "*.jpg", "*.jpeg", "*.gif"));

        File selectedFile = fileChooser.showOpenDialog(null); // Reemplaza 'null' por tu escenario actual

        if (selectedFile != null) {
            try {
                Image image = new Image(new FileInputStream(selectedFile));
                imageView.setImage(image); // Suponiendo que tienes un ImageView para mostrar el icono
            } catch (FileNotFoundException e) {
                e.printStackTrace(); // Maneja la excepción como consideres apropiado
            }
        }
    }

    @FXML
    private void handleDeletePlaylist() {
        Playlist selectedPlaylist = playlistTable.getSelectionModel().getSelectedItem();
        if (selectedPlaylist != null) {
            try {
                playlistDao.delete(selectedPlaylist.getName()); // Eliminar de la base de datos
                allPlaylists.remove(selectedPlaylist); // Eliminar de la lista observable
                playlistTable.refresh(); // Refrescar la TableView
            } catch (SQLException e) {
                e.printStackTrace();
                // Manejar excepción
            }
        } else {
            // Mostrar mensaje de que no hay selección
        }
    }

    public ObservableList<Playlist> getAllPlaylists() {
        return allPlaylists;
    }

}
