/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MP3.controller;

import MP3.Dao.PlaylistDao;
import MP3.Dao.PlaylistDaoImpl;
import MP3.Dao.TrackDao;
import MP3.Model.Playlist;
import MP3.Pool.MyDataSource;
import java.io.File;
import java.sql.SQLException;
import java.util.Date;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;




/**
 *
 * @author Yassine Ainous
 */
public class CreatePlaylistDialogController {

    @FXML
    private Button doneButton;
    @FXML
    private Button browserButton;
    @FXML
    private String iconoSeleccionado;
    @FXML
    private ImageView iconoImageView;
    @FXML
    private TextField nameField;
    @FXML
    private TextField observationField;

    private PlaylistsController playlistsController;

    public void setPlaylistsController(PlaylistsController controller) {
        this.playlistsController = controller;
    }


    @FXML
    private void seleccionarIcono() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg", "*.gif"));
        File file = fileChooser.showOpenDialog(browserButton.getScene().getWindow());

        if (file != null) {
            iconoSeleccionado = file.toURI().toString();
            Image image = new Image(iconoSeleccionado);
            // Asegúrate de que el ImageView iconoImageView existe en tu FXML
            iconoImageView.setImage(image);
        }
    }

    private ImageView createImageView(String url) {
        if (url == null || url.isEmpty()) {
            return null;
        }
        ImageView imageView = new ImageView(new Image(url));
        imageView.setFitHeight(30); // ajusta el tamaño según necesites
        imageView.setPreserveRatio(true);
        return imageView;
    }

    // Asumiendo que tienes setters en PlayListViewController para recibir los datos
    

    @FXML
    private void closePopup() {
        // Obtén la ventana (Stage) actual a través de uno de los componentes de la UI
        // Suponiendo que tienes un botón o algún otro componente FXML referenciado aquí
        Stage stage = (Stage) doneButton.getScene().getWindow();
        // Cierra la ventana
        stage.close();
    }
    
    // ...
private PlaylistDao playlistDao = PlaylistDaoImpl.getInstance();

    @FXML
private void onOkButtonClicked() {
    String name = nameField.getText();
    String observations = observationField.getText();

    if (!name.isEmpty()) {
        Playlist newPlaylist = new Playlist(name, new Date(), observations, iconoSeleccionado);
        try {
            playlistDao.create(newPlaylist);
            playlistsController.getAllPlaylists().add(newPlaylist);
            closePopup();
        } catch (SQLException e) {
            mostrarMensajeError("Error al insertar en la base de datos: " + e.getMessage());
        }
    } else {
        mostrarMensajeError("Debes ingresar un nombre para la playlist.");
    }
}

private void mostrarMensajeError(String mensaje) {
        Alert alerta = new Alert(Alert.AlertType.ERROR);
        alerta.setTitle("Error");
        alerta.setHeaderText(null);
        alerta.setContentText(mensaje);
        alerta.showAndWait();
    }


}
