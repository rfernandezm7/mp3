package MP3.controller;

import static MP3.utils.ConfigManager.*;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.*;

public class AudioSearchController {

    @FXML
    private Button btn_resumepauseSearch;

    // Grupo para los RadioButtons de las opciones de daltonismo
    @FXML
    public void initialize() {
        try { // Load the configuration
            loadConfg();
            int colorblind = Integer.parseInt(getConfg("colorblind"));
            boolean darkmode = Boolean.parseBoolean(getConfg("darkmode"));
            int fontsize = Integer.parseInt(getConfg("fontsize"));
        } catch (IOException e) {
            e.getMessage();
        }
    }
}
