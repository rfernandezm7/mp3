/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MP3.controller;

import MP3.Dao.PlaylistDao;
import MP3.Dao.PlaylistDaoImpl;
import MP3.Model.Playlist;
import java.io.File;
import java.sql.SQLException;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Yassine Ainous
 */
public class ModifyPlaylistDialogController {

    @FXML
    private TextField nameField;
    @FXML
    private TextField observationField;
    @FXML
    private ImageView iconoImageView;
    @FXML
    private String iconoSeleccionado;
    @FXML
    private Button browserButton;
    @FXML
    private Button UpdateButton;

    private PlaylistsController playlistsController;

    private PlaylistDao playlistDao;
    private Playlist currentPlaylist;
    private String originalName; // Guarda el nombre original al cargar la playlist

    public void setPlaylist(Playlist playlist) {
    this.currentPlaylist = playlist; // Asegúrate de que currentPlaylist se actualice correctamente
        this.originalName = playlist.getName(); // Guarda el nombre original

        // Rellenar los campos con los datos de la playlist
        nameField.setText(playlist.getName());
        observationField.setText(playlist.getObservations());

        // Verificar y establecer la imagen del ícono
        String iconUrl = playlist.getIcon();
        if (iconUrl != null && !iconUrl.isEmpty()) {
            try {
                iconoImageView.setImage(new Image(iconUrl));
            } catch (Exception e) {
                e.printStackTrace();
                // Maneja la excepción, por ejemplo, estableciendo una imagen predeterminada o dejándolo en blanco
            }
        } else {
            // Aquí puedes dejar el ImageView vacío o establecer una imagen predeterminada
            iconoImageView.setImage(null); // o setImage(imagenPredeterminada);
        }
    }

    public void setPlaylistsController(PlaylistsController controller) {
        this.playlistsController = controller;
    }

    public ModifyPlaylistDialogController() {
        // Aquí deberías obtener la instancia de tu PlaylistDao.
        // Asegúrate de que MyDataSource está correctamente configurada y puede devolver una instancia válida.
        this.playlistDao = PlaylistDaoImpl.getInstance();
    }

    @FXML
    private void seleccionarIcono() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg", "*.gif"));
        File file = fileChooser.showOpenDialog(browserButton.getScene().getWindow());

        if (file != null) {
            iconoSeleccionado = file.toURI().toString();
            Image image = new Image(iconoSeleccionado);
            // Asegúrate de que el ImageView iconoImageView existe en tu FXML
            iconoImageView.setImage(image);
        }
    }

    private ImageView createImageView(String url) {
        if (url == null || url.isEmpty()) {
            return null;
        }
        ImageView imageView = new ImageView(new Image(url));
        imageView.setFitHeight(30); // ajusta el tamaño según necesites
        imageView.setPreserveRatio(true);
        return imageView;
    }

    @FXML
public void onSaveButtonClicked() {
    String name = nameField.getText();
    String observations = observationField.getText();

    // Actualizar la playlist con los nuevos datos
    currentPlaylist.setName(name);
    currentPlaylist.setObservations(observations);
    currentPlaylist.setIcon(iconoSeleccionado); // Asegúrate de que este campo se actualice en seleccionarIcono()

    try {
        playlistDao.update(currentPlaylist, originalName); // Actualizar en la base de datos
        Stage stage = (Stage) UpdateButton.getScene().getWindow();
        stage.close();

        playlistsController.refreshTable(); // Refrescar la tabla en el controlador principal
    } catch (SQLException e) {
        e.printStackTrace();
        // Manejar la excepción aquí...
    }
}


}
