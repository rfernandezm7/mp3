package MP3.controller;

import MP3.Dao.PlaylistDaoImpl;
import MP3.Dao.TrackDaoImpl;
import MP3.Dao.TrackPlaylistDaoImpl;
import MP3.Model.ImageData;
import MP3.Model.Playlist;
import MP3.Model.Track;
import MP3.utils.ButtonToolTip;
import MP3.utils.FileUtils;
import MP3.utils.IdentificaOS;
import MP3.utils.ImageLogic;
import MP3.utils.ViewAlterators;
import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaException;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author raulf
 */
public class MainController implements Initializable {

    //Declaraciones Media
    Media media = null;
    MediaPlayer player = null;

    //Variables auxiliares
    String song = null;//Nombre cancion formateada
    String playlistName = null; //Nombre Playlist

    //Iconos
    Image iconImage = null;
    byte[] iconBytes = null;
    int height;
    int width;

    // Colección que almacena los nombres de las canciones
    ObservableList<String> nombreCancion = FXCollections.observableArrayList();

    //Datos Tabla
    ObservableList<Track> tracks = FXCollections.observableArrayList();
    //Almacenamos Playlists
    ObservableList<Playlist> playlists = FXCollections.observableArrayList();

    //Variables para Instanciar DAO
    private PlaylistDaoImpl playlistDao;
    private TrackDaoImpl trackDao;
    private TrackPlaylistDaoImpl trackPlaylistDao;

    //Booleano verificacion para saber si esta en likes o dislikes
    boolean isTrue = false;

    //Importaciones FXML
    @FXML
    private Text titleSong; //Nombre cancion que aparece reproductor;

    //Sin utilizar todavia
    @FXML
    private Button btn_changeToPlaylists;
    @FXML
    private Button btn_addFiles;
    @FXML
    private Button btn_audioSearch;

    @FXML
    private Button btn_settings;
    @FXML
    private Menu cm3;
    
    @FXML
    private ContextMenu cm1;
    @FXML
    private Label lbl_currentTime;
    @FXML
    private Slider barradetiempo;
    @FXML
    private Label lbl_totalTime;
    @FXML
    private Button btn_Stop;
    @FXML
    private Button btn_FB;
    @FXML
    private Button btn_PlayPause;
    @FXML
    private Button btn_FF;
    @FXML
    private Button btn_rattingP;
    @FXML
    private ImageView imageView; //Imagen icono cancion
    @FXML
    private Button btn_rattingN;
    @FXML
    private Button btn_Songs; //Boton Vista Tracks
    @FXML
    private Text titleSong; //Nombre cancion que aparece reproductor; 
    @FXML
    private TableView<Track> table2;
    @FXML
    private TableColumn<Track, String> iconColumn;
    @FXML
    private TableColumn<Track, String> titleColumn;
    @FXML
    private TableColumn<Track, String> artistColumn;
    @FXML
    private TableColumn<Track, String> observationColumn;
    //Text Barra Busquda
    @FXML
    private TextField searchBar;
    @FXML
    private Menu cm2;
    

    // Asegúrate de que esto esté correctamente vinculado en tu FXML
    /**
     * Initializes the controller class.
     *
     * @param url
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Configuramos texto alternativo botones reproductor
        ButtonToolTip.configureTooltip(btn_Stop);
        ButtonToolTip.configureTooltip(btn_FF);
        ButtonToolTip.configureTooltip(btn_PlayPause);
        ButtonToolTip.configureTooltip(btn_FB);
        ButtonToolTip.configureTooltip(btn_rattingP);
        ButtonToolTip.configureTooltip(btn_rattingN);

        //Instanciamos implementaciones DAO
        trackDao = trackDao.getInstance();
        playlistDao = playlistDao.getInstance();
        trackPlaylistDao = trackPlaylistDao.getInstance();
        
        //cargar playlists desde la base de datos
        playlists = loadPlaylistsFromDatabase();
        

        // Agregar playlists al ContextMenu
        addPlaylistsToContextMenu(playlists);
        
        //Logica para mostrar nuestro menu conceptual
        table2.setOnContextMenuRequested(event -> {
        
        //Show Spectacle
        cm1.show(table2, event.getScreenX(), event.getScreenY());
    });
    

        //inicialment tenim desactivat el botó de test
        this.btn_Stop.setDisable(true);
        this.btn_FF.setDisable(true);
        this.btn_PlayPause.setDisable(true);
        this.btn_FB.setDisable(true);
        this.btn_Stop.setDisable(true);
        this.btn_rattingP.setDisable(true);
        this.btn_rattingN.setDisable(true);
        this.btn_Songs.setDisable(true);

        //Carga de tablas y metodo reproducir al iniciar
        try {
            // Carga los datos iniciales en la TableView
            updateTableView();

            if (!tracks.isEmpty()) {
                //Inicialitzem reproductor amb mp3 de prova(Cambiar al LastSong Propertys
                Track firstTrack = tracks.get(0);

                //Abrimos el recurso en el reproductor
                openMedia(firstTrack.getFolderPath());

            } else {
                //Inicialitzem reproductor amb la canço default de l'aplicacció
                String path = FileUtils.getTestMP3(this);
                iniciaMedia(path);

            }
        } catch (SQLException e) {
            FileUtils.mostrarMensajeError("Error al cargar las tablas playlist");
        }

        //Un cop el reproductor està preparat, podem activar el botó per a procedir
        player.setOnReady(() -> {
            this.btn_FB.setDisable(false);
            this.btn_PlayPause.setDisable(false);
            this.btn_FF.setDisable(false);
            this.btn_Stop.setDisable(false);
            this.btn_rattingP.setDisable(false);
            this.btn_rattingN.setDisable(false);

            // Actualizar la UI según sea necesario
            lbl_totalTime.setText(formatTime(player.getMedia().getDuration(), player.getMedia().getDuration()));
        });
        //Si detenem el reproductor desactivem els botons stop y Like dislike Y activem Play y FForward FBackward
        player.setOnStopped(()
                -> {
            this.btn_FB.setDisable(false);
            this.btn_FF.setDisable(false);
            this.btn_PlayPause.setDisable(false);
            this.btn_Stop.setDisable(true);
            this.btn_rattingP.setDisable(true);
            this.btn_rattingN.setDisable(true);
        });
        //Si Pausamos el reproductor desactivamos el boton de pausa Y activamos el resto
        player.setOnPaused(() -> {
            this.btn_FB.setDisable(false);
            this.btn_FF.setDisable(false);
            this.btn_PlayPause.setDisable(false);
            this.btn_Stop.setDisable(true);
            this.btn_rattingP.setDisable(false);
            this.btn_rattingN.setDisable(false);

        });
// Carga los datos iniciales en la TableView
        try {
            updateTableView();
            if (!tracks.isEmpty()) {
                // Si hay pistas disponibles, selecciona la primera y reproduce
                Track firstTrack = tracks.get(0);

                openMedia(firstTrack.getFolderPath());
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // Manejar excepción adecuadamente, quizás mostrar un mensaje al usuario
            FileUtils.mostrarMensajeError("Error while loading the playlist tables");
        }
        //Seleccionamos Track 
        table2.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Track> observable, Track oldValue, Track newValue) -> {
            if (newValue != null) {
                //Activamos botones reproduccion
                this.btn_FB.setDisable(false);
                this.btn_PlayPause.setDisable(false);
                this.btn_FF.setDisable(false);
                this.btn_Stop.setDisable(false);
                this.btn_rattingP.setDisable(false);
                this.btn_rattingN.setDisable(false);

                // El nuevo valor (newValue) es el elemento seleccionado en la TableView y imprimimos la cancion seleccionada en los controles
                titleSong.setText(newValue.getTitle());

                //Asignamos el icono recogido de la base de datos;
                iconBytes = newValue.getIcon();
                height = newValue.getHeight();
                width = newValue.getWidth();
                //Dibujamos con el array de Bytes los pixeles para formar una Image
                iconImage = ImageLogic.bytesToImage(iconBytes, width, height);

                //La añadimos al ImageViewFXML
                imageView.setImage(iconImage);

                //Abrimos el nuevo recurso mp3 en el reproductor
                openMedia(newValue.getFolderPath());

                // Esdeveniment que si el reproductor esta en playing, posem la icona del stop
                player.setOnPlaying(() -> this.setButtonIcon(true));

                //Esdeveniment que si el reproductor està preparat, posem el de play
                player.setOnPaused(() -> this.setButtonIcon(false));

                ////Esdeveniment que si el reproductor està Stop, posem el de play
                player.setOnStopped(() -> this.setButtonIcon(false));
            }
        });
        //Esdeveniment que al finalitzar la canço pausa el reproductor i desactiva els botons
        player.setOnEndOfMedia(() -> {
            player.stop();
            this.btn_Stop.setDisable(true);
            this.btn_FF.setDisable(true);
            this.btn_PlayPause.setDisable(true);
            this.btn_FB.setDisable(true);
            this.btn_Stop.setDisable(true);
            this.btn_rattingP.setDisable(true);
            this.btn_rattingN.setDisable(true);
        });

        // esdeveniment que si el reproductor esta en playing, posem la icona del stop
        player.setOnPlaying(() -> this.setButtonIcon(true));

        //esdeveniment que si el reproductor està preparat, posem el de play
        player.setOnPaused(() -> this.setButtonIcon(false));

        //Esdeveniment que si el reproductor esta Stopped posem el de play;
        player.setOnStopped(() -> this.setButtonIcon(false));

    }

    //CAMBIOS DE VISTA 
    @FXML
    public void switchToPlaylistView(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/main/PlaylistsView.fxml"));
        ViewAlterators.switchView(loader, event);
    }

    public void switchToTracksView(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/main/MainView.fxml"));
        ViewAlterators.switchView(loader, event);
    }

    @FXML
    public void changetoAudioSearch(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/additionals/AudioSearchView.fxml"));
        ViewAlterators.newView(loader, "Audio Search", 600, 400);
    }

    @FXML
    public void changetoSettings(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/additionals/SettingsView.fxml"));
        ViewAlterators.newView(loader, "Settings", 600, 300);
    }

    @FXML
    private void addFiles(ActionEvent event) {
        Path selectedPath = FileUtils.selectFileToAdd();
        //Comprovem que tenim el Path del Archiu Seleccionat
        if (selectedPath == null) {
            FileUtils.mostrarMensajeError("No file found or there was an error while reading the file.");
            return;
        }
        //Fem una conversio per tindre una ruta Absoluta en String
        File mp3File = selectedPath.toFile();
        String path = mp3File.getAbsolutePath();

        //Extreure el titol de la canço en cas de no haber metadades;
        if (IdentificaOS.getOS() == IdentificaOS.TipusOS.WIN) {
            song = path.substring(path.lastIndexOf('\\') + 1, path.lastIndexOf('.'));
        } else if (IdentificaOS.getOS() == IdentificaOS.TipusOS.LINUX) {
            song = path.substring(path.lastIndexOf('/') + 1, path.lastIndexOf('.'));
        }
        //Ho guardem en una ObservaBlelist
        nombreCancion.add(song);

        //Obrim el recurs al reproductorMP3 per extreure metadates
        openMedia(path);

        //Fem un fil que mentres estigui el reproductor Inicialitzat extregui y enmagatzemi totes les dades a la base de dades
        player.setOnReady(new Runnable() {
            @Override
            public void run() {
                Map<String, Object> metadata = media.getMetadata();
                //Instanciamos un objeto ImageData para crear una Imagen proximamente
                ImageData dataimg = null;
                
                //Operadores terciarios para recoger metadatos en caso de no ser Null asignar valor Default
                
                String title = metadata.containsKey("title") ? String.valueOf(metadata.get("title")) : "Desconocido";
                String artist = metadata.containsKey("artist") ? String.valueOf(metadata.get("artist")) : "Desconocido";
                String observation = metadata.containsKey("comment") ? String.valueOf(metadata.get("comment")) : "Desconocido";

                if (metadata.containsKey("image")) {
                    iconImage = (Image) metadata.get("image");
                    dataimg = ImageLogic.imageToBytes(iconImage);
                } else {
                    // Establecer un valor predeterminado cargando un archivo mp3.png
                    try {
                        dataimg = ImageLogic.imageToBytes(new Image("icons/mp3.png"));

                    } catch (Exception e) {
                        FileUtils.mostrarMensajeError("Experienced an error while readind the metadata");
                    }
                }
                //Creem un nou Track
                Track track = new Track();
                //Li pasem totes les dades al model;
                track.setFolderPath(path);
                track.setArtist(artist);
                track.setTitle(!"Desconocido".equals(title) ? title : song);
                track.setIcon(dataimg.getBytes());
                track.setHeight(dataimg.getHeight());
                track.setWidth(dataimg.getWidth());
                track.setObservation(observation);
                track.setArtist(artist);

                // Guarda la pista en la base de datos
                try {
                    TrackDaoImpl trackDao = new TrackDaoImpl();
                    trackDao.add(track); //Añade la pista a la base de datos
                    updateTableView(); // Actualiza la tabla para reflejar los nuevos datos
                    FileUtils.mostrarMensajeConfirm("Canción añadida con exito !!!");

                } catch (SQLException e) {
                    e.printStackTrace();
                    FileUtils.mostrarMensajeError("Error while adding the track to the database");
                }
            }
        });

    }

    //Borrar track
    @FXML
    private void handleDeleteTrack(ActionEvent event) {
        // Obtener la pista seleccionada en la tabla
        Track selectedTrack = table2.getSelectionModel().getSelectedItem();

        if (selectedTrack != null) {
            try {
                // Eliminar la pista de la base de datos
                trackDao.delete(selectedTrack.getTrack_hash());
                FileUtils.mostrarMensajeConfirm("Cancion eliminada con exito!! ");
                // Actualizar la vista (eliminar la pista de la tabla)
                updateTableView();
            } catch (SQLException e) {
                FileUtils.mostrarMensajeError("Error, the track could not be deleted");
            }
        }
    }

    //Añadir track a la playlist
    @FXML
    private void addTrackToPlaylist(ActionEvent event) throws SQLException {
        //Seleccionamos Track
        Track selectedTrack = table2.getSelectionModel().getSelectedItem();
        //Seleccionamos Playlist (Falta modificar este campo)
        playlistName = "Hola";

        //Comprovació si recollim el track seleccionat
        if (selectedTrack != null)
        try {
            // Verificar si la pista ya está en la playlist
            if (!trackPlaylistDao.isTrackInPlaylist(playlistName, selectedTrack.getTrack_hash())) {
                // La pista no está en la playlist, así que la añadimos
                isTrue = false;
                //Añadimos la pista a la tabla relacional donde el track ya esta dentro de la playlist
                trackPlaylistDao.add(playlistName, selectedTrack.getTrack_hash());
            } else {
                // La pista ya está en la playlist y le mostramos error al usuario
                isTrue = true;
                FileUtils.mostrarMensajeError("The track was already in the playlist.");
            }
        } catch (SQLException e) {
            FileUtils.mostrarMensajeError("Error the database operation could not be completed");
        }
    }

    //Like/Dislike
    @FXML
    private void handleLikeButton(ActionEvent event) {
        handleLikeDislikeButton(event, "Like");
    }

    //Funcion Boton Like
    @FXML
    private void handleDislikeButton(ActionEvent event) {
        handleLikeDislikeButton(event, "Dislike");
    }

    private void handleLikeDislikeButton(ActionEvent event, String selector) {
        // Obtener la instancia de tu implementación TrackPlaylistDao
        TrackPlaylistDaoImpl trackDao = new TrackPlaylistDaoImpl();
        //Seleccionamos Track
        Track selectedTrack = table2.getSelectionModel().getSelectedItem();
        if (selectedTrack != null)
    try {
            String name = "";
            switch (selector) {
                case "Like" -> {
                    name = selector + "d Songs";
                }
                case "Dislike" -> {
                    name = selector + "d Songs";
                }
            }
            //Creamos playlist para que exista
            createPlaylistIfNotExists(name);
            // Verificar si la pista ya está en la playlist
            if (!trackDao.isTrackInPlaylist(name, selectedTrack.getTrack_hash())) {
                // La pista no está en la playlist, así que la añadimos
                trackDao.add(name, selectedTrack.getTrack_hash());

            } else {
                // La pista ya está en la playlist, maneja según sea necesario
                FileUtils.mostrarMensajeError("Track found already in the playlistS");
            }
        } catch (SQLException e) {
            FileUtils.mostrarMensajeError("Error the opertaion on the database could not be completed");
        }
    }
    //Crear Playlist si no existe

    private void createPlaylistIfNotExists(String playlistName) {
        playlistDao = PlaylistDaoImpl.getInstance();
        try {
            //Buscamos si el nombre de la playlist existe en la base de datos
            Playlist playlist = playlistDao.findByName(playlistName);

            // La lista de reproducción no existe, así que la creamos
            if (playlist == null) {
                Date fechaActual = new Date();
                playlist = new Playlist(playlistName, fechaActual, "this is the " + playlistName + " playlist", "icons.png");
                playlistDao.create(playlist);
            }
        } catch (SQLException e) {
            FileUtils.mostrarMensajeError("Error the opertaion on the database could not be completed");
        }
    }

    //Actualizar tabla con datos
    private void updateTableView() throws SQLException {

        // Limpia la lista observable actual y agrega todas las nuevas pistas
        tracks.clear(); // Limpia la lista observable que está vinculada a tu TableView
        tracks.addAll(trackDao.getAll()); // Agrega todas las nuevas pistas a la lista observable

        // Configura la TableView para usar la ObservableList como su fuente de datos
        table2.setItems(tracks);

        //Columnas configuradas
        iconColumn.setCellValueFactory(new PropertyValueFactory<>("icon"));
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        artistColumn.setCellValueFactory(new PropertyValueFactory<>("artist"));
        observationColumn.setCellValueFactory(new PropertyValueFactory<>("observation"));

    }

    @FXML
    public void Pause_Play(ActionEvent event) {
        // Si apreten botó mentre el reproductor encara està en reproducció, el parem
        if (player.getStatus() != MediaPlayer.Status.PLAYING) {
            player.play();
        } else {
            player.pause();
        }
    }

    @FXML
    public void Stop(ActionEvent event) {
        if (player.getStatus() != MediaPlayer.Status.STOPPED) {
            player.stop();
        }
    }

    @FXML
    public void FastForwards(ActionEvent event) {
        Duration FFDuration = new Duration(10 * 1000);
        player.seek(player.getCurrentTime().add(FFDuration));
    }

    @FXML
    public void FastBackwards(ActionEvent event) {
        Duration FBDuration = new Duration(-10 * 1000);
        player.seek(player.getCurrentTime().add(FBDuration));
    }

    private String formatTime(Duration elapsed, Duration duration) {
        int intElapsed = (int) Math.floor(elapsed.toSeconds());
        int elapsedHours = intElapsed / (60 * 60);
        if (elapsedHours > 0) {
            intElapsed -= elapsedHours * 60 * 60;
        }
        int elapsedMinutes = intElapsed / 60;
        int elapsedSeconds = intElapsed - elapsedHours * 60 * 60
                - elapsedMinutes * 60;

        if (duration.greaterThan(Duration.ZERO)) {
            int intDuration = (int) Math.floor(duration.toSeconds());
            int durationHours = intDuration / (60 * 60);
            if (durationHours > 0) {
                intDuration -= durationHours * 60 * 60;
            }
            int durationMinutes = intDuration / 60;
            int durationSeconds = intDuration - durationHours * 60 * 60
                    - durationMinutes * 60;

            if (durationHours > 0) {
                return String.format("%d:%02d:%02d/%d:%02d:%02d",
                        elapsedHours, elapsedMinutes, elapsedSeconds,
                        durationHours, durationMinutes, durationSeconds);
            } else {
                return String.format("%02d:%02d/%02d:%02d",
                        elapsedMinutes, elapsedSeconds, durationMinutes,
                        durationSeconds);
            }
        } else {
            if (elapsedHours > 0) {
                return String.format("%d:%02d:%02d", elapsedHours,
                        elapsedMinutes, elapsedSeconds);
            } else {
                return String.format("%02d:%02d", elapsedMinutes,
                        elapsedSeconds);
            }
        }
    }

    private void setButtonIcon(boolean play) {
        if (play) {
            // btn_PlayPause.setGraphic(imgPause);
            btn_PlayPause.setText("⏸");
        } else {
            // btn_PlayPause.setGraphic(imgPlay);
            btn_PlayPause.setText("▶");
        }
    }

    private void iniciaMedia(String path) {
        try {
            // actuaslitzem el recurs MP3
            this.media = new Media(path);

            // inicialitzem el reproductor
            this.player = new MediaPlayer(media);
        } catch (MediaException e) {
            FileUtils.mostrarMensajeError("Error experienced opening the file: " + path + ":" + e.toString());
        }
    }

    private void openMedia(String path) {
        try {
            // Convertir la ruta de archivo a una URI
            String uriString = new File(path).toURI().toString();

            // Actualizamos el recurso MP3
            this.media = new Media(uriString);

            // Inicializamos el reproductor
            this.player = new MediaPlayer(media);
        } catch (MediaException e) {
            FileUtils.mostrarMensajeError("Error experienced opening the file: " + path + ":" + e.toString());
        }
        //Esdeveniments que actualizan el label y el Slider del Reproductor MP3
        player.setOnReady(() -> {
            barradetiempo.setMax(player.getMedia().getDuration().toSeconds());
            barradetiempo.setValue(0.0);
            lbl_totalTime.setText(formatTime(Duration.ZERO, player.getMedia().getDuration()));
        });
        player.setOnReady(() -> {
            barradetiempo.setMin(0.0);
            barradetiempo.setMax(player.getMedia().getDuration().toSeconds());
            barradetiempo.setValue(0.0);
            lbl_totalTime.setText(formatTime(player.getMedia().getDuration(), player.getMedia().getDuration()));

            // Actualizar el Slider y la etiqueta de tiempo actual cada vez que cambia currentTime.
            player.currentTimeProperty().addListener((obs, oldTime, newTime) -> {
                if (!barradetiempo.isValueChanging()) {
                    barradetiempo.setValue(newTime.toSeconds());
                    lbl_currentTime.setText(formatTime(newTime, player.getMedia().getDuration()));
                }
            });
        });

    }

    //Inicializacio Media amb canço del Repositori default
    private void iniciaMedia(String path) {
        try {
            // actuaslitzem el recurs MP3
            this.media = new Media(path);

            // inicialitzem el reproductor
            this.player = new MediaPlayer(media);
        } catch (MediaException e) {
            FileUtils.mostrarMensajeError("ERROR obrint fitxer demo: " + path + ":" + e.toString());
        }
    }

    //Filtro de buscar
    @FXML
    private void searchFilter(ActionEvent event) {
        //Recogemos texto de la barra de busqueda
        String keySearch = searchBar.getText();

        try {
            //Llamamos al método de la base de datos searchFilter
            ObservableList<Track> filteredTracks = FXCollections.observableArrayList(trackDao.searchFilter(keySearch));
                
            if (!filteredTracks.isEmpty()) {
                
                //Refrescamos la tabla con el filtro aplicado
                tracks.setAll(filteredTracks);
                FileUtils.mostrarMensajeConfirm("Busqueda completa!!");
            } else {
                FileUtils.mostrarMensajeError("No se ha encontrado su busqueda");
            }

        } catch (SQLException ex) {
            FileUtils.mostrarMensajeError("Error en la base de datos");
        }
    }
  //Cargar las playlists y recogerlas en una observableList
    public ObservableList<Playlist> loadPlaylistsFromDatabase() {
        ObservableList<Playlist> playlists = FXCollections.observableArrayList();

        try {
            List<Playlist> allPlaylists = playlistDao.getAll();
            playlists.addAll(allPlaylists);
        } catch (SQLException e) {
            // Manejar la excepción según sea necesario
            FileUtils.mostrarMensajeError("Eror al recoger las Playlist de la base de Datos");
        }

        return playlists;
    }
    //Add track To Playlist
    @FXML
    private void addTrackToPlaylist(ActionEvent event) {
    // Lógica para agregar la pista a la playlist específica (usando el nombre)
    MenuItem menuItem =  (MenuItem) event.getSource();
    
   //PlaylistSeleccionada
    playlistName = menuItem.getText();
    
    // Obtén la pista seleccionada
    Track selectedTrack = table2.getSelectionModel().getSelectedItem();
    
    // Comprobación si recogemos el track seleccionado
    if (selectedTrack != null) {
        try {
            // Verificar si la pista ya está en la playlist
            if (!trackPlaylistDao.isTrackInPlaylist(playlistName, selectedTrack.getTrack_hash())) {
                // La pista no está en la playlist, así que la añadimos
                trackPlaylistDao.add(playlistName, selectedTrack.getTrack_hash());
                FileUtils.mostrarMensajeConfirm("Pista agregada a la playlist: " + playlistName);
            } else {
                // La pista ya está en la playlist y le mostramos un mensaje de error al usuario
                FileUtils.mostrarMensajeError("La pista ya está en la playlist.");
            }
        } catch (SQLException e) {
            FileUtils.mostrarMensajeError("Error al operar con la base de datos");
        }
    }
}
 // CREAR MENU ITEMS CON EL NOMBRE PLAYLISTS
   private void addPlaylistsToContextMenu(ObservableList<Playlist> playlists) {
        for (Playlist playlist : playlists) {
            //Creacion de objetos Menu Items con nombre de las listas Playlist
            MenuItem playlistMenuItem = new MenuItem(Playlist.getName());
            
            //Le damos la funcion addTrackToPlaylist a cada submenu con nombre de Playlist Creada
            playlistMenuItem.setOnAction(event -> addTrackToPlaylist(event));
            
            //Le pasamos al Submenu de "addtrack.. "los items creados
            cm2.getItems().add(playlistMenuItem);
        }
    }
}
