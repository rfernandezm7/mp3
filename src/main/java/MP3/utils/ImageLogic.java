/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MP3.utils;

import MP3.Model.ImageData;
import javafx.scene.image.Image;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

/**
 *
 * @author raulf
 */
public class ImageLogic {

    public static Image bytesToImage(byte[] imageBytes, int width, int height) {
        try {
            // Verificar que las dimensiones sean válidas
            if (width <= 0 || height <= 0) {
                FileUtils.mostrarMensajeError("Dimensiones de la imagen no validas");
            }

            // Verificar que los bytes de la imagen no sean nulos o estén vacíos
            if (imageBytes == null || imageBytes.length == 0) {
                throw new IllegalArgumentException("Bytes de la imagen no válidos");
            }

            // Crear una nueva WritableImage
            WritableImage writableImage = new WritableImage(width, height);

            // Obtener el PixelWriter
            PixelWriter pixelWriter = writableImage.getPixelWriter();

            // Escribir los bytes de la imagen en la WritableImage
            pixelWriter.setPixels(0, 0, width, height, PixelFormat.getByteBgraInstance(), imageBytes, 0, width * 4);

            // Retornar la WritableImage creada
            return writableImage;
        } catch (IllegalArgumentException e) {
            // Manejar la excepción, por ejemplo, mostrar un mensaje de error
            FileUtils.mostrarMensajeError("Error al convertir bytes a imagen: " + e.getMessage());
            return null; // o manejar el error de alguna manera
        }
    }

    public static ImageData imageToBytes(Image image) {
        try {
            if (image == null) {
                FileUtils.mostrarMensajeError("Error: la imagen no puede ser nula");
            }

            PixelReader reader = image.getPixelReader();
            int width = (int) image.getWidth();
            int height = (int) image.getHeight();
            byte[] buffer = new byte[width * height * 4]; // Suponiendo una imagen de 4 bytes por píxel

            reader.getPixels(0, 0, width, height, PixelFormat.getByteBgraInstance(), buffer, 0, width * 4);

            return new ImageData(buffer, width, height);
        } catch (IllegalArgumentException e) {
            // Manejar la excepción, por ejemplo, mostrar un mensaje de error
            FileUtils.mostrarMensajeError("Error al convertir imagen a bytes: " + e.getMessage());
            return null;
        } catch (NullPointerException e) {
            FileUtils.mostrarMensajeError("Error No hay ninguna imagen");
            return null;
        }
    }

    public static Image pathToImage(String path) {
        try {
            if (path == null || path.isEmpty()) {
                throw new IllegalArgumentException("Error: la ruta de la imagen no puede ser nula o vacía");
            }

            return new Image(path);
        } catch (IllegalArgumentException e) {
            // Manejar la excepción, por ejemplo, mostrar un mensaje de error
            FileUtils.mostrarMensajeError("Error al cargar imagen desde la ruta: " + e.getMessage());
            return null;
        } catch (NullPointerException e) {
            FileUtils.mostrarMensajeError("Error No hay ninguna ruta");
            return null; // o manejar el error de alguna manera
        }
    }
}
