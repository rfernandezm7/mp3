package MP3.utils;

import MP3.Model.Playlist;
import MP3.controller.ModifyPlaylistDialogController;
import static MP3.utils.ConfigManager.getConfg;
import static MP3.utils.ConfigManager.loadConfg;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * @author alex
 */
public class ViewAlterators {

    public static void switchView(FXMLLoader loader, ActionEvent event) {
        try {
            // Cargamos el nuevo archivo FXML
            Parent root = loader.load();

            // Obtenemos el stage actual y lo reemplazamos con el nuevo
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);

            loadCSS(scene);

            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error al cambiar a PlaylistView: " + e.getMessage());
        }
    }

    public static void newView(FXMLLoader loader, String name, int size1, int size2) {
        try {
            // Cargamos el nuevo archivo FXML
            Parent settings = loader.load();
            Scene scene = new Scene(settings, size1, size2);

            // Obtenemos el stage actual y lo reemplazamos con el nuevo
            Stage stage = new Stage();
            stage.setTitle(name);

            loadCSS(scene);

            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            System.out.println("Error experienced while loading the "+name+" FXML");
            System.out.println(e.toString());
        }
    }

    public static void loadCSS(Scene scene) {
        try {
            //Cargamos la configuracion antes de abrir
            loadConfg();
            int colorblind = Integer.parseInt(getConfg("colorblind"));
            boolean darkmode = Boolean.parseBoolean(getConfg("darkmode"));
            int fontsize = Integer.parseInt(getConfg("fontsize"));

            // comprobamos si hay que aplicar estilos
            if (darkmode) {
                scene.getStylesheets().add("/style/CSSdarkmode.css");
            } else {
                switch (colorblind) {
                    case 0 -> {
                        scene.getStylesheets().add("/style/CSSstandard.css");
                    }
                    case 1 -> {
                        scene.getStylesheets().add("/style/CSScolorblindD.css");
                    }
                    case 2 -> {
                        scene.getStylesheets().add("/style/CSScolorblindP.css");
                    }
                    case 3 -> {
                        scene.getStylesheets().add("/style/CSScolorblindT.css");
                    }
                }
            }
            switch (fontsize) {
                case 12 -> {
                    scene.getStylesheets().add("/style/fontsize/CSSfontsize12.css");
                }
                case 16 -> {
                    scene.getStylesheets().add("/style/fontsize/CSSfontsize16.css");
                }
                case 20 -> {
                    scene.getStylesheets().add("/style/fontsize/CSSfontsize20.css");
                }
            }
        } catch (IOException e) {
            System.out.println("Error experienced while loading the settings FXML");
            System.out.println(e.toString());
        }
    }
}
