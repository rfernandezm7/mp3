package MP3.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Grupo 4
 */
public class FileUtils {

    /**
     * *
     *
     * @param url
     *
     * @return Si la URL és de tipus Windows, llavors elimina el nom de la
     * unitat
     *
     */
    public static String normalizeURLFormat(String url) {
        String ret = "";

        if (IdentificaOS.getOS() == IdentificaOS.TipusOS.WIN) {
            ret = url.replace("[A-Z]{1}:", "");
        }

        return ret;
    }

    /**
     * *
     * Retorna la ruta al MP3 de prova dels recursos. La ruta es torna en format
     * URL. P.ex:
     *
     * Si s'executa sobre Windows, ho retorna en el format:
     * file:/C:/Users/manel/.../target/classes/sounds/test_sound.mp3 Si
     * s'executa sobre Linux, ho retorna en el format:
     * file:/home/manel/.../target/classes/sounds/test_sound.mp3
     *
     * https://en.wikipedia.org/wiki/File_URI_scheme
     *
     * @param instance instància des de la qual es crida (this)
     * @return
     */
    public static String getTestMP3(Object instance) {
        return instance.getClass().getClassLoader().getResource("sounds/Travis-Scott-Ft.-Bad-Bunny-Y-The-Weeknd-K-POP.mp3").toString();
    }

    /**
     * *
     * Retorna una icona dels recursos
     *
     * @param instance
     * @param nom
     * @return
     */
    public static String getIcona(Object instance, String nom) {
        return instance.getClass().getClassLoader().getResource("icons/" + nom).toString();
    }

    /**
     * *
     * Permet seleccionar un fitxer MP3 d'una unitat del disc
     *
     * @return
     */
    public static Path selectFileToAdd() {

        Path ret = null;

        Stage stage1 = new Stage();

        FileChooser filechooser1 = new FileChooser();
        filechooser1.getExtensionFilters().add(new FileChooser.ExtensionFilter("MP3 files", "*.mp3"));

        filechooser1.setTitle("Seleccionar fitxer MP3");
        File selectedFile = filechooser1.showOpenDialog(stage1);

        try {
            if (selectedFile != null) {
                Path fitxerMP3 = selectedFile.toPath();
                if (isValidMp3File(fitxerMP3)) {
                    ret = fitxerMP3;
                } else {
                    mostrarMensajeError("Error: No es un archivo MP3 válido");
                }
            } else {
                mostrarMensajeError("Error: No se seleccionó ningún archivo");
            }
        } catch (NullPointerException e) {
            mostrarMensajeError("Error: No hay ningún archivo");
        }

        return ret;
    }
    //Popup de Error
    public static void mostrarMensajeError(String mensaje) {
        Alert alerta = new Alert(Alert.AlertType.ERROR);
        alerta.setTitle("Error");
        alerta.setHeaderText(null);
        alerta.setContentText(mensaje);
        alerta.showAndWait();
    }
    //Popup Confirmación
    public static void mostrarMensajeConfirm(String mensaje) {
        Alert alerta = new Alert(Alert.AlertType.INFORMATION);
        alerta.setTitle("Error");
        alerta.setHeaderText(null);
        alerta.setContentText(mensaje);
        alerta.showAndWait();
    }
    
    private static boolean isValidMp3File(Path path) {
    boolean isValid = false;

    try (FileInputStream fis = new FileInputStream(path.toFile())) {
        byte[] buffer = new byte[3];
        fis.read(buffer, 0, 3);

        String header = new String(buffer, StandardCharsets.ISO_8859_1);
        isValid = header.equals("ID3") || (buffer[0] == (byte) 0xFF && (buffer[1] & 0xE0) == 0xE0);

        if (!isValid) {
            mostrarMensajeError("Error: No es un archivo MP3 válido");
        }

    } catch (IOException e) {
        mostrarMensajeError("Error: Al cargar el fichero");
    
    }

    return isValid;
    }
}

