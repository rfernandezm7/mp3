package MP3.utils;

import MP3.Model.Playlist;
import MP3.Model.Track;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Properties;

/**
 *
 * @author Grupo 4
 */
public class ConfigManager {

    private static final File CONFGFILE = new File(System.getProperty("user.home") + "/user.confg");
    static Properties properties = new Properties();

    public static void loadConfg() throws IOException {
        if (CONFGFILE.exists()) {
            try {
                properties.load(new FileInputStream(CONFGFILE));
            } catch (IOException e) {
                 FileUtils.mostrarMensajeError("Exception generated on load confgfile");
                throw e;
            }
        } else {
            File backupFile = new File("src/main/resources/confg/user.confg.bkp");
            try {
                Files.copy(backupFile.toPath(), CONFGFILE.toPath());
            } catch (IOException e) {
                 FileUtils.mostrarMensajeError("Exception generated on copy confgfile");
                throw e;
            }
        }
    }

   /* public static void saveLastPlayed() throws IOException {
        try {
            if (Track.getTrack_hash() ==null) {
                saveConfg("playlist", "default");
                saveConfg("track", "");
            } else {
                saveConfg("playlists", Playlist.getName());
                saveConfg("track", Track.getTrack_hash()));
            }
        } catch (IOException e) {
            throw e;
        }
    }*/

    public static void saveConfg(String key, String value) throws IOException {
        try ( OutputStream output = new FileOutputStream(CONFGFILE)) {
            properties.setProperty(key, value);
            properties.store(output, "Configuration file (this file can be deleted to restore the default values)");
        } catch (IOException | NullPointerException e) {
            FileUtils.mostrarMensajeError("Exception generated on saveConfg");
            throw e;
        }
    }

    public static String getConfg(String key) throws IOException {
        try ( InputStream input = new FileInputStream(CONFGFILE)) {
            properties.load(input);
            return properties.getProperty(key);
        } catch (IOException | NullPointerException e) {
             FileUtils.mostrarMensajeError("Exception generated on getConfg");
            throw e;
        }
    }
}
