package MP3.utils;

import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;

/**
 *
 * @author Grupo 4
 */
public class ButtonToolTip {
    //Texto de infomracion flotante para botones
    public static void configureTooltip(Button button) {
        Object userData = button.getUserData();
        if (userData != null) {
            Tooltip tooltip = new Tooltip(userData.toString());
            button.setTooltip(tooltip);
        }
    }

}
