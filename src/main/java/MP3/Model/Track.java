/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MP3.Model;


import java.util.Arrays;
import java.util.Objects;

/**
 *
 * @author Yassine Ainous
 */
public class Track {
    
   private static int track_hash;
   private String title ;
   private String artist ;
   private byte[]  icon ;
   private String observation;
   private String folder_path;
   private int height;
   private int width;

    public Track(int track_hash, String title, String artist, byte [] icon, String observation, String folder_path, int height, int width) {
        Track.track_hash = track_hash;
        this.title = title;
        this.artist = artist;
        this.icon = icon;
        this.observation = observation;
        this.folder_path = folder_path;
        this.height= height;
        this.width= width;
    }

    public Track() {
    }
    

    public int getTrack_hash() {
        return track_hash;
    }

    public void setTrack_hash(int track_hash) {
       Track.track_hash = track_hash;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public byte[] getIcon() {
        return icon;
    }

    public void setIcon(byte[] icon) {
        this.icon = icon;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.title);
        hash = 11 * hash + Objects.hashCode(this.artist);
        hash = 11 * hash + Arrays.hashCode(this.icon);
        hash = 11 * hash + Objects.hashCode(this.observation);
        hash = 11 * hash + Objects.hashCode(this.folder_path);
        hash = 11 * hash + this.height;
        hash = 11 * hash + this.width;
        return hash;
    }

    public String getFolder_path() {
        return folder_path;
    }

    public void setFolder_path(String folder_path) {
        this.folder_path = folder_path;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getFolderPath() {
        return folder_path;
    }

    public void setFolderPath(String folder_path) {
        this.folder_path = folder_path;
    }

    @Override
    public String toString() {
        return "Track{" + "title=" + title + ", artist=" + artist + ", icon=" + icon + ", observation=" + observation + ", folder_path=" + folder_path + ", height=" + height + ", width=" + width + '}';
    }

   

   
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Track other = (Track) obj;
        if (!this.observation.equals(other.observation)) {
            return false;
        }
        if (!Objects.equals(Track.track_hash, Track.track_hash)) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.artist, other.artist)) {
            return false;
        }
        if (!Objects.equals(this.icon, other.icon)) {
            return false;
        }
        return Objects.equals(this.folder_path, other.folder_path);
    }
        
}
