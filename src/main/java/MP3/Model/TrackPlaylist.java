/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MP3.Model;

import java.util.Objects;

public class TrackPlaylist {
    private static String name;
    private int track_hash;

    public TrackPlaylist(String name, int track_hash) {
        TrackPlaylist.name = name;
        this.track_hash = track_hash;
    }

    public String getName() {
        return name;
    }

    public static void setName(String name) {
        TrackPlaylist.name = name;
    }

    public int getTrack_hash() {
        return track_hash;
    }

    public void setTrack_hash(int track_hash) {
        this.track_hash = track_hash;
    }

    @Override
    public String toString() {
        return "TrackPlaylist{" + "name=" + name + ", track_hash=" + track_hash + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.name);
        hash = 17 * hash + this.track_hash;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        TrackPlaylist other = (TrackPlaylist) obj;
        return Objects.equals(this.name, other.name) && this.track_hash == other.track_hash;
    }
}
