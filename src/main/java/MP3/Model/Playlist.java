package MP3.Model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Yassine Ainous
 */
public class Playlist {

    private static String name;
    private Date creation;
    private String observations;
    private String icon;
    private List<Track> tracks; // Nueva lista de pistas;

    public List<Track> getTracks() {
        return tracks;
    }

    public void setTracks(List<Track> tracks) {
        this.tracks = tracks;
    }

    public Playlist(String name, Date creation, String observations, String icon) {
        Playlist.name = name;
        this.creation = creation;
        this.observations = observations;
        this.icon = icon;
        this.tracks = new ArrayList<>();

    }

    public Playlist() {
    }
    

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        Playlist.name = name;
    }

    public Date getCreation() {
        return creation;
    }

    public void setCreation(Date creation) {
        this.creation = creation;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "Playlist{" + "name=" + name + ", creation=" + creation + ", observations=" + observations + ", icon=" + icon + '}';
    }

   

   

}
