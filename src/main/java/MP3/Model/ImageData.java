/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MP3.Model;

/**
 *
 * @author raulf
 */
public class ImageData {

          private final byte[] bytes;
          private final int width;
          private final int height;

          public ImageData(byte[] bytes, int width, int height) {
                    this.bytes = bytes;
                    this.width = width;
                    this.height = height;
          }

          public byte[] getBytes() {
                    return bytes;
          }
          public int getWidth() {
                    return width;
          }

          public int getHeight() {
                    return height;
          }

}
