package MP3.main;

import MP3.Pool.MyDataSource;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import MP3.utils.ViewAlterators;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/main/MainView.fxml"));
        ViewAlterators.newView(loader, "MediaSound", 892, 568);
    }

    /* @Override
    public void stop() {
        try {
            saveLastPlayed();
        } catch (IOException e) {
            e.getMessage();
        }
    }*/
    /**
     * *
     * Mètode que inicialitza l'aplicació JFX No s'utilitzen els arguments
     * "args"
     *
     * @param args
     */
    public static void main(String[] args) throws SQLException {
        try ( Connection conn = MyDataSource.getConnection()) {
            System.out.println("Conectado");
        } catch (SQLException e) {
            System.out.println("no esta conectado ");
            e.printStackTrace();
        }
        launch(args);
    }
}
